import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class main {
    public static void main (String [] args){
        System.out.println("hello shell. Read ");
        ProcessBuilder processBuilder = new ProcessBuilder();

        //invoking shell (bash) and giving it the command
        processBuilder.command("bash", "-c", "cd ~ ; cp */shellinjava/printhis.txt Downloads/printhis.txt; less */shellinjava/printhis.txt");

        try {

            Process process = processBuilder.start();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            int exitCode = process.waitFor();
            System.out.println("\nExited with error code : " + exitCode);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
