# Shell in java

This programme invokes the shell to send a test file to the local machine and print it live.  The process builder class is used here to trigger the system call.

## Installation

Use the following command to clone test project to a local machine.

```bash
git clone https://gitlab.com/YMSUpeksha/shellinjava.git
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
TDD recommended. 

## Pre-requisites

This programme is specifically written for the linux operating systems. Therefore, without minor alterations to the process builder, it is not guranteed to successfully work in windows machines.
IDE - preferably IntelliJ IDEA, Eclipse | netbeans\
[JRE - ](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) Java runtime environment according to the operating system.


